//
// Created by expert on 26-08-17.
//

#ifndef RANGETEST_RANDOMGENERATOR_HPP
#define RANGETEST_RANDOMGENERATOR_HPP

template< typename RandomNumberDistribution, typename RandomNumberGenerator >
class RandomGenerator
{
	public:
		RandomGenerator( RandomNumberDistribution& distribution, RandomNumberGenerator& rng )
				:distribution( distribution ), rng( rng )
		{
		}

		double operator()( void )
		{
			return distribution( rng );
		}

	private:
		RandomNumberDistribution& distribution;
		RandomNumberGenerator& rng;
};


#endif //RANGETEST_RANDOMGENERATOR_HPP
