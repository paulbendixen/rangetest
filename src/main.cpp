#include <iostream>
#include <random>
#include <range/v3/all.hpp>

#include "RandomGenerator.hpp"

int main()
{
	std::random_device dev;
	std::mt19937 noise( dev() );
	std::normal_distribution<> distribution( 1.0, .05 );

	RandomGenerator< std::normal_distribution<>, std::mt19937 > gen( distribution, noise );
	auto noiseGen = ranges::view::generate( gen );
	std::cout << ( noiseGen | ranges::view::take( 10 ) ) << '\n';
	std::cout << ( ranges::view::transform( noiseGen, noiseGen, []( auto a, auto b ){ return ( a + b )/2; } ) |
			ranges::view::take( 20 ) );
	return 0;
}